import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Location } from "./Location";
import { Enums } from "./Enums";
import { Image } from "./Image";
import { Track } from "./Track";

@Index("device_pk", ["id"], { unique: true })
@Index("device_location_id_idx", ["locationId"], {})
@Index("organization_uid_un", ["organizationId", "uid"], { unique: true })
@Entity("device", { schema: "public" })
export class Device extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "integer", name: "id" })
  id!: number;

  @Column("integer", { name: "organization_id", unique: true })
  organizationId!: number;

  @Column("character varying", { name: "uid", unique: true, length: 20 })
  uid!: string;

  @Column("integer", { name: "location_id" })
  locationId!: number;

  @Column("jsonb", { name: "metadata", nullable: true })
  metadata!: object | null;

  @ManyToOne(() => Location, (location) => location.devices, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "location_id", referencedColumnName: "id" }])
  location!: Location;

  @ManyToOne(() => Enums, (enums) => enums.devices)
  @JoinColumn([{ name: "organization_id", referencedColumnName: "id" }])
  organization!: Enums;

  @ManyToOne(() => Enums, (enums) => enums.devices2)
  @JoinColumn([{ name: "type_id", referencedColumnName: "id" }])
  type!: Enums;

  @OneToMany(() => Image, (image) => image.device)
  images!: Image[];

  @OneToMany(() => Track, (track) => track.device)
  tracks!: Track[];
}
