import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Classification } from "./Classification";
import { Detection } from "./Detection";
import { User } from "./User";

@Index("user_label_pk", ["id"], { unique: true })
@Entity("user_label", { schema: "public" })
export class UserLabel extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "integer", name: "id" })
  id!: number;

  @Column("smallint", { name: "confidence" })
  confidence!: number;

  @Column("timestamp without time zone", { name: "create_timestamp" })
  createTimestamp!: Date;

  @Column("timestamp without time zone", {
    name: "modified_timestamp",
    nullable: true,
  })
  modifiedTimestamp!: Date | null;

  @Column("jsonb", { name: "metadata", nullable: true })
  metadata!: object | null;

  @ManyToOne(
    () => Classification,
    (classification) => classification.userLabels,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "classification_id", referencedColumnName: "id" }])
  classification!: Classification;

  @ManyToOne(() => Detection, (detection) => detection.userLabels, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "detection_id", referencedColumnName: "id" }])
  detection!: Detection;

  @ManyToOne(() => User, (user) => user.userLabels, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  @JoinColumn([
    { name: "user_id", referencedColumnName: "id" },
    { name: "user_id", referencedColumnName: "id" },
  ])
  user!: User;
}
