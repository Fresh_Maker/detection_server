import {
  BaseEntity,
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { DetectionRun } from "./DetectionRun";

@Index("detector_pk", ["id"], { unique: true })
@Index("name_version_un", ["name", "version"], { unique: true })
@Entity("detector", { schema: "public" })
export class Detector extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "integer", name: "id" })
  id!: number;

  @Column("character varying", { name: "name", unique: true, length: 60 })
  name!: string;

  @Column("character varying", { name: "version", unique: true, length: 60 })
  version!: string;

  @Column("jsonb", { name: "metadata", nullable: true })
  metadata!: object | null;

  @OneToMany(() => DetectionRun, (detectionRun) => detectionRun.detector)
  detectionRuns!: DetectionRun[];
}
