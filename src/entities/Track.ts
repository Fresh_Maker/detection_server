import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Classification } from "./Classification";
import { Device } from "./Device";
import { TrackDetections } from "./TrackDetections";

@Index("track_pk", ["id"], { unique: true })
@Entity("track", { schema: "public" })
export class Track extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "integer", name: "id" })
  id!: number;

  @Column("timestamp without time zone", { name: "first_image_timestamp" })
  firstImageTimestamp!: Date;

  @Column("timestamp without time zone", { name: "last_image_timestamp" })
  lastImageTimestamp!: Date;

  @Column("jsonb", { name: "metadata", nullable: true })
  metadata!: object | null;

  @ManyToOne(() => Classification, (classification) => classification.tracks, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "classification_id", referencedColumnName: "id" }])
  classification: Classification = new Classification;

  @ManyToOne(() => Device, (device) => device.tracks, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "device_id", referencedColumnName: "id" }])
  device: Device = new Device;

  @OneToMany(() => TrackDetections, (trackDetections) => trackDetections.track)
  trackDetections!: TrackDetections[];
}
