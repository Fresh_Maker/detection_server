import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Detection } from "./Detection";
import { Detector } from "./Detector";
import { Image } from "./Image";

@Index("detection_run_create_timestamp_idx", ["createTimestamp"], {})
@Index("detection_run_pk", ["id"], { unique: true })
@Entity("detection_run", { schema: "public" })
export class DetectionRun extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "integer", name: "id" })
  id!: number;

  @Column("timestamp without time zone", { name: "create_timestamp" })
  createTimestamp!: Date;

  @Column("integer", { name: "state" })
  state!: number;

  @Column("timestamp without time zone", {
    name: "start_timestamp",
    nullable: true,
  })
  startTimestamp!: Date | null;

  @Column("timestamp without time zone", {
    name: "end_timestamp",
    nullable: true,
  })
  endTimestamp!: Date | null;

  @Column("jsonb", { name: "metadata", nullable: true })
  metadata!: object | null;

  @OneToMany(() => Detection, (detection) => detection.detectionRun)
  detections!: Detection[];

  @ManyToOne(() => Detector, (detector) => detector.detectionRuns, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "detector_id", referencedColumnName: "id" }])
  detector!: Detector;

  @ManyToOne(() => Image, (image) => image.detectionRuns, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "image_id", referencedColumnName: "id" }])
  image!: Image;
}
