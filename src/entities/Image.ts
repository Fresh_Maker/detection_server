import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { DetectionRun } from "./DetectionRun";
import { Device } from "./Device";

@Index("device_timestamp_un", ["deviceId", "timestamp"], { unique: true })
@Index("image_pk", ["id"], { unique: true })
@Index("resource_path_un", ["resourcePath"], { unique: true })
@Entity("image", { schema: "public" })
export class Image extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "integer", name: "id" })
  id!: number;

  @Column("integer", { name: "device_id", unique: true })
  deviceId!: number;

  @Column("timestamp without time zone", { name: "timestamp", unique: true })
  timestamp!: Date;

  @Column("smallint", { name: "timezone" })
  timezone!: number;

  @Column("character varying", {
    name: "resource_path",
    unique: true,
    length: 200,
  })
  resourcePath!: string;

  @Column("jsonb", { name: "metadata", nullable: true })
  metadata!: object | null;

  @OneToMany(() => DetectionRun, (detectionRun) => detectionRun.image)
  detectionRuns!: DetectionRun[];

  @ManyToOne(() => Device, (device) => device.images, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "device_id", referencedColumnName: "id" }])
  device: Device = new Device;
}
