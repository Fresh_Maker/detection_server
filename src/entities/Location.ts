import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Device } from "./Device";
import { Enums } from "./Enums";

@Index("location_pk", ["id"], { unique: true })
@Entity("location", { schema: "public" })
export class Location extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "integer", name: "id" })
  id!: number;

  @Column("smallint", { name: "timezone" })
  timezone!: number;

  @Column("point", { name: "coordinates", nullable: true })
  coordinates!: string | object | null;

  @Column("jsonb", { name: "metadata", nullable: true })
  metadata!: object | null;

  @OneToMany(() => Device, (device) => device.location)
  devices!: Device[];

  @ManyToOne(() => Enums, (enums) => enums.locations)
  @JoinColumn([{ name: "name_id", referencedColumnName: "id" }])
  name!: Enums;
}
