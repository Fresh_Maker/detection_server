import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Classification } from "./Classification";
import { Device } from "./Device";
import { LabelTask } from "./LabelTask";
import { Location } from "./Location";


@Index("enums_id_pk", ["id"], { unique: true })
@Entity("enums", { schema: "public" })
export class Enums extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "integer", name: "id" })
  id!: number;

  @Column("character varying", { name: "value", length: 200 })
  value!: string;

  @Column("character varying", { name: "type", length: 60 })
  type!: string;

  @Column("character varying", { name: "code", length: 60 })
  code!: string;

  @Column("jsonb", { name: "metadata", nullable: true })
  metadata!: object | null;

  @OneToMany(() => Classification, (classification) => classification.name)
  classifications!: Classification[];

  @OneToMany(() => Device, (device) => device.organization)
  devices!: Device[];

  @OneToMany(() => Device, (device) => device.type)
  devices2!: Device[];

  @OneToMany(() => LabelTask, (labelTask) => labelTask.minimumExpertise)
  labelTasks!: LabelTask[];

  @OneToMany(() => LabelTask, (labelTask) => labelTask.priority)
  labelTasks2!: LabelTask[];

  @OneToMany(() => LabelTask, (labelTask) => labelTask.status)
  labelTasks3!: LabelTask[];

  @OneToMany(() => Location, (location) => location.name)
  locations!: Location[];

  // @OneToMany(() => UserOrganization, userOrganiztion=>userOrganiztion.organization)
  // userOrganizations!: UserOrganization[];

  // @OneToMany(() => UserRole, userRole=>userRole.role)
  // userRoles!: UserRole[];
}
