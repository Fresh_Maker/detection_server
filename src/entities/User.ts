import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Enums } from "./Enums";
import { UserLabel } from "./UserLabel";

@Index("user_pk", ["id"], { unique: true })
@Entity("user", { schema: "public" })
export class User extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "integer", name: "id" })
  id!: number;

  @Column("character varying", { name: "name", length: 200 })
  name!: string;

  @Column("character varying", { name: "email", length: 200 })
  email!: string;

  @Column("character varying", { name: "password", length: 200 })
  password!: string;

  @Column("jsonb", { name: "metadata", nullable: true })
  metadata!: object | null;

  @OneToMany(() => UserLabel, (userLabel) => userLabel.user)
  userLabels!: UserLabel[];

  @ManyToMany(() => Enums)
  @JoinTable({
    name: "user_organization",
    joinColumn: {
      name: "user_id",
    },
    inverseJoinColumn: {
      name: "organization_id",
    },
  })
  organizations!: Enums[];

  @ManyToMany(() => Enums)
  @JoinTable({
    name: "user_role",
    joinColumn: {
      name: "user_id",
    },
    inverseJoinColumn: {
      name: "role_id",
    },
  })
  roles!: Enums[];

}
