import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Detection } from "./Detection";
import { Enums } from "./Enums";

@Index("label_task_pk", ["id"], { unique: true })
@Entity("label_task", { schema: "public" })
export class LabelTask extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "integer", name: "id" })
  id!: number;

  @Column("timestamp without time zone", { name: "create_timestamp" })
  createTimestamp!: Date;

  @Column("timestamp without time zone", {
    name: "modified_timestamp",
    nullable: true,
  })
  modifiedTimestamp!: Date | null;

  @Column("jsonb", { name: "metadata", nullable: true })
  metadata!: object | null;

  @ManyToOne(() => Detection, (detection) => detection.labelTasks, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "detection_id", referencedColumnName: "id" }])
  detection!: Detection;

  @ManyToOne(() => Enums, (enums) => enums.labelTasks)
  @JoinColumn([{ name: "minimum_expertise", referencedColumnName: "id" }])
  minimumExpertise!: Enums;

  @ManyToOne(() => Enums, (enums) => enums.labelTasks2)
  @JoinColumn([{ name: "priority", referencedColumnName: "id" }])
  priority!: Enums;

  @ManyToOne(() => Enums, (enums) => enums.labelTasks3)
  @JoinColumn([{ name: "status", referencedColumnName: "id" }])
  status!: Enums;
}
