import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Detection } from "./Detection";
import { Track } from "./Track";

@Index("detection_id_idx", ["detectionId"], {})
@Index("track_detections_pk", ["detectionId", "trackId"], { unique: true })
@Index("track_id_idx", ["trackId"], {})
@Entity("track_detections", { schema: "public" })
export class TrackDetections extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "integer", name: "id" })
  id!: number;

  @Column("integer", { primary: true, name: "track_id" })
  trackId!: number;

  @Column("integer", { primary: true, name: "detection_id" })
  detectionId!: number;

  @ManyToOne(() => Detection, (detection) => detection.trackDetections, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "detection_id", referencedColumnName: "id" }])
  detection: Detection = new Detection;

  @ManyToOne(() => Track, (track) => track.trackDetections, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "track_id", referencedColumnName: "id" }])
  track: Track = new Track;
}
