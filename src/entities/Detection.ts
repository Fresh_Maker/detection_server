import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Classification } from "./Classification";
import { DetectionRun } from "./DetectionRun";
import { LabelTask } from "./LabelTask";
import { TrackDetections } from "./TrackDetections";
import { UserLabel } from "./UserLabel";

@Index("detection_detection_class_id_idx", ["classificationId"], {})
@Index("detection_detection_run_id_idx", ["detectionRunId"], {})
@Index("detection_pk", ["id"], { unique: true })
@Index("detection_box_idx", ["roi"], {})
@Entity("detection", { schema: "public" })
export class Detection extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "integer", name: "id" })
  id!: number;

  @Column("integer", { name: "detection_run_id" })
  detectionRunId!: number;

  @Column("geometry", { name: "roi" })
  roi!: string;

  @Column("real", {
    name: "detection_confidence",
    nullable: true,
    precision: 24,
  })
  detectionConfidence!: number | null;

  @Column("integer", { name: "classification_id" })
  classificationId!: number;

  @Column("real", {
    name: "classification_confidence",
    nullable: true,
    precision: 24,
  })
  classificationConfidence!: number | null;

  @Column("jsonb", { name: "classification_distribution", nullable: true })
  classificationDistribution!: object | null;

  @Column("character varying", { name: "crop_resource_path", length: 200 })
  cropResourcePath!: string;

  @Column("jsonb", { name: "metadata", nullable: true })
  metadata!: object | null;

  @ManyToOne(
    () => Classification,
    (classification) => classification.detections,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  @JoinColumn([{ name: "classification_id", referencedColumnName: "id" }])
  classification!: Classification;

  @ManyToOne(() => DetectionRun, (detectionRun) => detectionRun.detections, {
    onDelete: "CASCADE",
    onUpdate: "CASCADE",
  })
  @JoinColumn([{ name: "detection_run_id", referencedColumnName: "id" }])
  detectionRun!: DetectionRun;

  @OneToMany(() => LabelTask, (labelTask) => labelTask.detection)
  labelTasks!: LabelTask[];

  @OneToMany(
    () => TrackDetections,
    (trackDetections) => trackDetections.detection
  )
  trackDetections!: TrackDetections[];

  @OneToMany(() => UserLabel, (userLabel) => userLabel.detection)
  userLabels!: UserLabel[];
}
