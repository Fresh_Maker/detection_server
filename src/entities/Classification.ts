import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Enums } from "./Enums";
import { Detection } from "./Detection";
import { Track } from "./Track";
import { UserLabel } from "./UserLabel";

@Index("classification_pk", ["id"], { unique: true })
@Entity("classification", { schema: "public" })
export class Classification extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "integer", name: "id" })
  id!: number;

  @Column("jsonb", { name: "metadata", nullable: true })
  metadata!: object | null;

  @ManyToOne(() => Enums, (enums) => enums.classifications)
  @JoinColumn([{ name: "name_id", referencedColumnName: "id" }])
  name!: Enums;

  @OneToMany(() => Detection, (detection) => detection.classification)
  detections!: Detection[];

  @OneToMany(() => Track, (track) => track.classification)
  tracks!: Track[];

  @OneToMany(() => UserLabel, (userLabel) => userLabel.classification)
  userLabels!: UserLabel[];
}
