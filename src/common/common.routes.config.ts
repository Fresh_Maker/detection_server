import * as express from "express";
export abstract class Common_Routes_Config {
  app: express.Application;
  name: string;

  constructor(app: express.Application, name: string) {
    this.app = app;
    this.name = name;
    this.configure_routes();
  }
  getName() {
    return this.name;
  }
  abstract configure_routes(): express.Application;
}
