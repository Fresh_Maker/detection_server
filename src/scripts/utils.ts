export const print_json = (name: string, circ: Object,) => {
  // Note: cache should not be re-used by repeated calls to JSON.stringify.
  let cache: any = [];
  const obj_string = JSON.stringify(circ, (key, value) => {
    if (typeof value === "object" && value !== null) {
      // Duplicate reference found, discard key
      if (cache.includes(value)) return;

      // Store value in our collection
      cache.push(value);
    }
    return value;
  },1);
  cache = null; // Enable garbage collection

  console.log(name + "\n" + obj_string);
};
