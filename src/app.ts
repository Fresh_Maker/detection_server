import express = require("express");
import http  = require("http");
import cors = require("cors");
import { Common_Routes_Config } from "./common/common.routes.config";
import { Users_Routes } from "./users/users.routes.config";
import { Enums_Routes } from "./enums/enums.routes.config";
import debug from "debug";
import 'module-alias/register';
import 'source-map-support/register';
import "reflect-metadata";
import { createConnection } from "typeorm";
import compression = require("compression");
import helmet = require("helmet");
import winston = require("winston");
import expressWinston = require('express-winston');

const app: express.Application = express();
const server: http.Server = http.createServer(app);
const port = 3000;
const routes: Array<Common_Routes_Config> = [];
const debugLog: debug.IDebugger = debug("app");

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());
app.use(helmet());
// compress all responses
app.use(compression())

app.use(
  expressWinston.logger({
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.timestamp(),
      winston.format.json({space: 1})
    ),
  })
);

routes.push(new Users_Routes(app));
routes.push(new Enums_Routes(app));

app.use(
  expressWinston.errorLogger({
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.json()
    ),
  })
);

app.get("/", (req: express.Request, res: express.Response) => {
  res.status(200).send(`Server running at http://localhost:${port}`);
});

server.listen(port, () => {
  debugLog(`Server running at http://localhost:${port}`);
  routes.forEach((route: Common_Routes_Config) => {
    debugLog(`Routes configured for ${route.getName()}`);
  });
  createConnection().then(() => {
    console.log('db connected');
    
  })
});

