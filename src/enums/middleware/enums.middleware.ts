import * as express from "express";
import enumService from "../services/enums.service";
import debug from "debug";

const log: debug.IDebugger = debug("app:enums-controller");
class Enums_Middleware {
  async validate_required_enum_body_fields(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    if (req.body && req.body.code && req.body.type && req.body.value) {
      next();
    } else {
      res
        .status(400)
        .send({ error: `Missing required fields code and type and value` });
    }
  }

  // TODO: JL add checks for UPPERCASE, and min/max of all values
  async validate_enum_body_fields(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    if (req.body && req.body.code && req.body.type && req.body.value) {
      next();
    } else {
      res
        .status(400)
        .send({ error: `Missing required fields code and type and value` });
    }
  }

  async validate_enum_exists(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const enums = await enumService.read(req.params.enumId);
    if (enums) {
      next();
    } else {
      res.status(404).send({ error: `Enum ${req.params.enumId} not found` });
    }
  }

  async extract_enum_id(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    req.body.id = req.params.enumId;
    next();
  }

  async extract_enum_type(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    req.body.type = req.params.type;
    next();
  }
}

export default new Enums_Middleware();
