import { Common_Routes_Config } from "../common/common.routes.config";
import Enums_Controller from "./controllers/enums.controller";
import Enums_Middleware from "./middleware/enums.middleware";
import * as express from "express";

export class Enums_Routes extends Common_Routes_Config {
  constructor(app: express.Application) {
    super(app, "EnumsRoutes");
  }

  configure_routes() {
    this.app
      .route(`/enums`)
      .get(Enums_Controller.list_enums)
      .post(
        Enums_Middleware.validate_required_enum_body_fields,
        Enums_Controller.create_enum
      );

    this.app
      .route(`/enums/type_list`)
      .get(Enums_Controller.get_enums_by_type_list)
    
    this.app.param(`type`, Enums_Middleware.extract_enum_id);
    this.app
      .route(`/enums/type/:type`)
      .get(Enums_Controller.get_enums_by_type_list)

    this.app.param(`enumId`, Enums_Middleware.extract_enum_id);
    this.app
      .route(`/enums/:enumId`)
      .all(Enums_Middleware.validate_enum_exists)
      .get(Enums_Controller.get_enum_by_id)
      .delete(Enums_Controller.remove_enum);

    this.app.put(`/enums/:enumId`, [
      Enums_Middleware.validate_required_enum_body_fields,
      Enums_Controller.put,
    ]);

    return this.app;
  }
}
