import * as express from "express";
import * as argon2 from "argon2";
import debug from "debug";
import enumsService from "src/enums/services/enums.service";

const log: debug.IDebugger = debug("app:enums-controller");
class Enums_Controller {
  async list_enums(req: express.Request, res: express.Response) {
    const enums = await enumsService.list(100, 0);
    console.log(enums);
    res.status(200).send(enums);
  }

  async get_enum_by_id(req: express.Request, res: express.Response) {
    const enums = await enumsService.read(req.params.enumId);
    res.status(200).send(enums);
  }

  async create_enum(req: express.Request, res: express.Response) {
    delete req.body.id;
    await enumsService
      .create(req.body)
      .then((result) => {
        res.status(201).send({ item: result });
      })
      .catch((error) => {
        res.status(200).send({ error });
      });
  }

  async put(req: express.Request, res: express.Response) {
    const result = await enumsService.update({
      id: req.params.enumId,
      ...req.body,
    });
    log(result);
    res.status(200).send({ item: result });
  }

  async remove_enum(req: express.Request, res: express.Response) {
    await enumsService
      .delete(req.params.enumId)
      .then((result) => {
        res.status(201).send({ item: result });
      })
      .catch((error) => {
        res.status(200).send({ error });
      });
  }

  async get_enums_by_type_list(req: express.Request, res: express.Response) {
    const enum_list = await enumsService.get_by_type(req.params.type);
    log(enum_list);
    res.status(200).send(enum_list);
  }

  async get_by_type_list(req: express.Request, res: express.Response) {
    const type_list = await enumsService.get_all_types();
    log(type_list);
    res.status(200).send(type_list);
  }
}

export default new Enums_Controller();
