import { CRUD } from "src/common/interfaces/crud.interface";
import { Enums } from "src/entities/Enums";
import "reflect-metadata";
import { I_enum } from "src/entities/interfaces/database_interface";

export class EnumsService implements CRUD {
  async create(resource: I_enum) {
    const enums = new Enums();
    enums.code = resource.code;
    enums.type = resource.type;
    enums.value = resource.value;
    return enums.save();
  }

  async delete(resourceId: string) {
    const enums = await Enums.findOne({ id: parseInt(resourceId) });
    return enums?.remove();
  }

  async list(take: number, page: number) {
    const skip = page * take;
    const [list, total] = await Enums.findAndCount({ take, skip });
    return { list, total };
  }

  async read(resourceId: string) {
    return Enums.findOne({ id: parseInt(resourceId) });
  }

  async update(resource: I_enum) {
    const enums = await Enums.findOne({ id: resource.id });
    if (enums) {
      enums.code = resource.code;
      enums.type = resource.type;
      enums.value = resource.value;
      return enums.save();
    }
    return {} as Enums;
  }

  async get_all_types(): Promise<string[]> {
    const enums = await Enums.find();
    let result = [] as string[];
    if (enums){
      const result_set = enums.reduce((acc, enuum) => {
        acc.add(enuum.type);
        return acc;
      }, new Set());
      result = Array.from(result_set) as string[];
    }
    return result;
  }

  async get_by_type(type:string): Promise<Enums[]> {
    const result = await Enums.find({where: {type}});
    return result;
  }
}

export default new EnumsService();
