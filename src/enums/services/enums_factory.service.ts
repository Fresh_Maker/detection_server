import { CRUD } from "src/common/interfaces/crud.interface";
import { Enum } from "src/entities/Enums";
import "reflect-metadata";
import { I_enum } from "src/entities/interfaces/database_interface";
import { EnumsService } from "./enums.service";

export class EnumsFactoryService extends EnumsService {
  type: string;

  constructor(type: string){
    super();
    this.type = type;
  }

  async create(resource: I_enum) {
    const enums = new Enum();
    enums.code = resource.code;
    enums.type = this.type;
    enums.value = resource.value;
    return enums.save();
  }

  async list(take: number=10, page: number=0) {
    const skip = page * take;
    const [list, total] = await Enum.findAndCount({ 
      where:{
        type: this.type
    },
    take, skip });
    return { list, total };
  }

  async update(resource: I_enum) {
    const self = this;
    const enums = await Enum.findOne({ id: resource.id });
    if (enums) {
      enums.code = resource.code;
      enums.type = self.type;
      enums.value = resource.value;
      return enums.save();
    }
    return null;
  }
}
