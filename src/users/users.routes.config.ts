import { Common_Routes_Config } from "../common/common.routes.config";
import Users_Controller from "./controllers/users.controller";
import Users_Middleware from "./middleware/users.middleware";
import * as express from "express";

export class Users_Routes extends Common_Routes_Config {
  constructor(app: express.Application) {
    super(app, "UsersRoutes");
  }

  configure_routes() {
    this.app
      .route(`/users`)
      .get(Users_Controller.list_users)
      .post(
        Users_Middleware.validate_required_user_body_fields,
        Users_Middleware.validate_same_email_doesnt_exist,
        Users_Controller.create_user
      );

    this.app.param(`userId`, Users_Middleware.extract_user_id);
    this.app
      .route(`/users/:userId`)
      .all(Users_Middleware.validate_user_exists)
      .get(Users_Controller.get_user_by_id)
      .delete(Users_Controller.remove_user);

    this.app.put(`/users/:userId`, [
      Users_Middleware.validate_required_user_body_fields,
      Users_Middleware.validate_same_email_belong_to_same_user,
      Users_Controller.put,
    ]);

    return this.app;
  }
}
