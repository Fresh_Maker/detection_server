import { CRUD } from "src/common/interfaces/crud.interface";
import { User } from "src/entities/User";

import "reflect-metadata";
import { I_user } from "src/entities/interfaces/database_interface";
import { Enums } from "src/entities/Enums";



class UsersService implements CRUD {
  async create(resource: I_user) {
    const user = new User();
    user.name = resource.name || "";
    user.email = resource.email;
    user.password = resource.password;
    return user.save();
  }

  async delete(resourceId: string) {
    const user = await User.findOne({ id: parseInt(resourceId) });
    return user?.remove();
  }

  async list(take: number, page: number, relations: any = []) {
    const skip = page * take;
    const [list, total] = await User.findAndCount({ take, skip, relations });
    return { list, total };
  }

  async read(resourceId: string, relations: any = []) {
    return User.findOne({ id: parseInt(resourceId) }, { relations });
  }

  async update(resource: I_user) {
    const user = await User.findOne({ id: resource.id });
    if (user) {
      user.name = resource.name || "";
      user.email = resource.email;
      user.password = resource.password;
      const role_id_list = resource.roles.map((role) => role.id);
      user.roles = await Enums.findByIds(role_id_list);

      const org_id_list = resource.organizations.map((org) => org.id);
      user.organizations = await Enums.findByIds(org_id_list);

      // user.organizations = resource.organizations;
      return user.save();
    }
    return null;
  }

  async get_user_by_email(email: string) {
    return User.findOne({ email });
  }
}

export default new UsersService();
