import * as express from "express";
import * as argon2 from "argon2";
import debug from "debug";
import usersService from "src/users/services/users.service";
import { I_option } from "src/common/interfaces/interface";
import {getManager} from "typeorm";

const log: debug.IDebugger = debug("app:users-controller");
class Users_Controller {
  async list_users(req: express.Request, res: express.Response) {
    const users = await usersService.list(100, 0, ["roles", "organizations"]);
    res.status(200).send(users);
  }

  async get_user_by_id(req: express.Request, res: express.Response) {
    const result = await usersService.read(req.params.userId, ["roles", "organizations"]);    
    res.status(200).send({ item: result });
  }

  async create_user(req: express.Request, res: express.Response) {
    req.body.password = await argon2.hash(req.body.password);
    const result = await usersService.create(req.body);
    res.status(201).send({ item: result });
  }


  async put(req: express.Request, res: express.Response) {
    req.body.password = await argon2.hash(req.body.password);
    const user = await usersService.update({ id: req.params.userId, ...req.body })
    log(user);
    res.status(204).send({item: user});
  }

  async remove_user(req: express.Request, res: express.Response) {
    const result = await usersService.delete(req.params.userId);
    log(result);
    res.status(204).send({ item: result });
  }
}

export default new Users_Controller();
